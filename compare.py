#!/usr/bin/env python3

import os
import sys

from glob import glob
from pathlib import Path
from shutil import rmtree

from subprocess import run

def sh(cmd):
    print('+', cmd)
    run(cmd, shell=True, check=True)


def sh_redirect(dpath, cmd):
    os.makedirs(dpath, exist_ok=True)
    sh(f'{cmd} > {dpath}/stdout 2> {dpath}/stderr ; echo $? > {dpath}/exitstatus')

def process(ex, args):
    sh(f'clang++ -I. -std=c++14 tests/{ex}.cpp -o tests/{ex}')
    for arg in args:
        argstr = arg.replace(' ', '_')
        sh_redirect(f'tmp/top/cxx/{ex}-{argstr}', f'tests/{ex} {arg}')
        sh_redirect(f'tmp/top/python/{ex}-{argstr}', f'tests/{ex}.py {arg}')


CASES = [
    ('tests/ex*.py',
     (
         '-h',
         '--unknown',
         'aaa bbb ccc',
         '--repeat=999',
         '--repeat',
         '--repeat 888',
         '--repeat=not_a_number',
         'alpha',
         'alpha beta',
         'alpha beta gamma',
     )),

    ('tests/subcmds_ex*.py',
     (
         '--verbose',
         '--unknown',
         '--verbose what',
         '--verbose make',
         '--verbose make -h',
         '--verbose make --parallel',
         '--verbose make --parallel 123',

     )),
]

def main():
    if os.path.isdir('tmp/top'):
        rmtree('tmp/top')
    os.makedirs('tmp/top')

    for pattern, args in CASES:
        for ex_py in glob(pattern):
            ex_py = Path(ex_py)
            ex = ex_py.stem
            process(ex, args)


main()
