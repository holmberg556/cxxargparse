
#include "CxxArgparse.h"

#include <iostream>

int main(int argc, char* argv[])
{
  auto o = CxxArgparse("ex1", "Say hello");
  auto verbose = o.Bool("verbose", "be more verbose").Short('v');

  auto infile = o.Arg("infile", "the input file");
  auto outfile = o.Arg("outfile", "the output file");
  
  auto args = o.Parse(argc, argv);

  std::cout << "infile=" << *infile << "\n";
  std::cout << "outfile=" << *outfile << "\n";
  
  return 0;
}
