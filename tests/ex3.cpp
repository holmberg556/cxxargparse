
#include "CxxArgparse.h"

#include <iostream>

int main(int argc, char* argv[])
{
  auto o = CxxArgparse("ex1", "Say hello");
  auto verbose = o.Bool("verbose", "be more verbose").Short('v');
  auto debug = o.Bool("debug", "enable debug output");
  auto repeat = o.Int("repeat", "repeat greeting N times").Metavar("N");

  auto debug1 = o.Bool("debug1");
  auto debug2 = o.Bool("debug2", "enable debug output");
  auto debug3 = o.Bool("debugxx3", "enable debug output");
  auto debug4 = o.Bool("debug4", "enable debug output");
  auto debug5 = o.Bool("debugxxxxxxxxxxx5", "enable debug output output output output output output output output");

  
  auto args = o.Parse(argc, argv);

  std::cout << "verbose=" << (*verbose ? "True" : "False") << "\n";

#if 0

  auto verbose = o.Bool("verbose", "be more verbose");
  auto parallel = o.Int("parallel", "run in parallel").Default(99);
  auto incdir = o.Str("incdir", "include directory").Short('I');
  auto define = o.Strs("define", "preprroecessor derfines");

  auto make = o.Bool("make", "make all").OneOf("xxx");
  auto clean = o.Bool("clean", "clean all").OneOf("xxx");


  auto input = o.Arg("input", "the input file");
  auto output = o.Arg("output", "the output file");

  auto args = o.Parse(argc, argv);

  for (int i = 0; i < args.size(); i++) {
    std::cout << "args[" << i << "] = " << args[i] << "\n";
  }

  std::cout << "\n";
  std::cout << "verbose: " << *verbose << "\n";
  std::cout << "parallel: " << *parallel + 1000000 << "\n";
  std::cout << "incdir: " << *incdir + " ..."
            << "\n";

  for (int i = 0; i < (*define).size(); i++) {
    std::cout << "define[" << i << "] = " << (*define)[i] << "\n";
  }
#endif  
  
  return 0;
}
