#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description='Say hello', prog='ex1')

parser.add_argument('-v', '--verbose', action='store_true', help='be more verbose')

parser.add_argument('infile', help='the input file')
parser.add_argument('outfile', nargs='*', help='the output file')

args = parser.parse_args()

print(f'infile={args.infile}')
for x in args.outfile:
    print(f'outfile={x}')
