
#include "CxxArgparse.h"

#include <iostream>

int main(int argc, char* argv[])
{
  auto o = CxxArgparse("ex1", "Say hello");
  auto verbose = o.Bool("verbose", "be more verbose").Short('v');

  auto debug1 = o.Bool("debug1");
  auto debug2 = o.Bool("debug2");
  auto debug3 = o.Bool("debug3");
  auto debug4 = o.Bool("debug4");

  auto input = o.Arg("infile", "the input file");
  auto output = o.Arg("outfile", "the output file");
  
  auto args = o.Parse(argc, argv);
  
  std::cout << "verbose=" << (*verbose ? "True" : "False") << "\n";

  return 0;
}
