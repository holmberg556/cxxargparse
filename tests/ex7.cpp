
#include "CxxArgparse.h"

#include <iostream>

int main(int argc, char* argv[])
{
  auto o = CxxArgparse("ex1", "Say hello");
  auto verbose = o.Bool("verbose", "be more verbose").Short('v');

  auto infile = o.Arg("infile", "the input file");
  auto outfile = o.Args("outfile", "the output file").Nargs('*');
  
  auto args = o.Parse(argc, argv);

  std::cout << "infile=" << *infile << "\n";
  for (auto & x : *outfile) {
    std::cout << "outfile=" << x << "\n";
  }
  
  return 0;
}
