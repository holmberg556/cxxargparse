
#include "CxxArgparse.h"

#include <iostream>

int main(int argc, char* argv[])
{
  auto o = CxxArgparse("ex1", "Say hello");
  auto verbose = o.Bool("verbose", "be more verbose").Short('v');

  auto infile = o.Arg("infile", "the input file").Nargs('?');
  auto outfile = o.Arg("outfile", "the output file").Nargs('?');
  
  auto args = o.Parse(argc, argv);

  std::cout << "verbose=" << (*verbose ? "True" : "False") << "\n";
  
  if (infile) {
    std::cout << "infile=" << *infile << "\n";
  }
  else {
    std::cout << "infile=" << "None" << "\n";
  }

  if (outfile) {
    std::cout << "outfile=" << *outfile << "\n";
  }
  else {
    std::cout << "outfile=" << "None" << "\n";
  }
  
  return 0;
}
