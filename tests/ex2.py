#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description='Say hello', prog='ex1')

parser.add_argument('-v', '--verbose', action='store_true', help='be more verbose')
parser.add_argument('--debug', action='store_true', help='enable debug output')
parser.add_argument('-R', '--repeat', type=int, metavar='N', action='store', help='repeat greeting N times')

parser.add_argument('--debug1', action='store_true')
parser.add_argument('--debug2', action='store_true', help='enable debug output')
parser.add_argument('--debugxx3', action='store_true', help='enable debug output')
parser.add_argument('--debug4', action='store_true', help='enable debug output')
parser.add_argument('--debugxxxxxxxxxxxxx5', action='store_true', help='enable debug output output output output output output output output')

args = parser.parse_args()

print(f'verbose={args.verbose}')
print(f'repeat={args.repeat}')
