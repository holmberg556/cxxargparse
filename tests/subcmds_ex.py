#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description='Say hello', prog='ex1')

parser.add_argument('-v', '--verbose', action='store_true', help='be more verbose')
parser.add_argument('--debug', action='store_true', help='enable debug output')
parser.add_argument('--repeat', type=int, metavar='N', action='store', help='repeat greeting N times')

subparsers = parser.add_subparsers(help='sub-command help')

# create the parser for the "a" command
parser_make = subparsers.add_parser('make', help='make help')
parser_make.add_argument('--parallel', type=int, metavar='N', help='parallel help')

# create the parser for the "b" command
parser_clean = subparsers.add_parser('clean', help='clean help')
parser_clean.add_argument('--all', action='store_true', help='all help')

args = parser.parse_args()

print(f'verbose={args.verbose}')
print(f'repeat={args.repeat}')

print()
if 'parallel' in args:
    print(f'parallel={args.parallel}')
else:
    print(f'parallel=None')
