
#include "CxxArgparse.h"

#include <iostream>

int main(int argc, char* argv[])
{
  auto o = CxxArgparse("ex1", "Say hello");
  auto verbose = o.Bool("verbose", "be more verbose").Short('v');
  auto debug = o.Bool("debug", "enable debug output");
  auto repeat = o.Int("repeat", "repeat greeting N times").Metavar("N");

  auto make_cmd = o.Subcmd("make", "Say hello");
  auto parallel = make_cmd.Int("parallel", "parallel help").Metavar("N");

  auto clean_cmd = o.Subcmd("clean", "...");
  auto all = clean_cmd.Bool("all", "more files");

  auto args = o.Parse(argc, argv);

  std::cout << "verbose=" << (*verbose ? "True" : "False") << "\n";
  if (repeat) {
    std::cout << "repeat=" << *repeat << "\n";
  }
  else {
    std::cout << "repeat=" << "None" << "\n";
  }
  std::cout << "\n";
  if (parallel) {
    std::cout << "parallel=" << (*parallel) << "\n";
  }
  else {
    std::cout << "parallel=" << "None" << "\n";
  }

  return 0;
}
