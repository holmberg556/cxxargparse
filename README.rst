
Using CxxArgparse.h
===================

``CxxArgparse.h`` can parse the command line arguments of a C++ program.
It is modelled after the Python module ``argparse``. An example::

    #include "CxxArgparse.h"
    #include <iostream>

    int main(int argc, char* argv[])
    {
      // parse command line
      auto o = CxxArgparse("demoprog", "Demonstrate use of CxxArgparse");

      auto verbose = o.Bool("verbose",  "be more verbose").Short('v');
      auto debug   = o.Bool("debug",    "enable debug output");
      auto repeat  = o.Int("repeat",    "repeat things N times").Metavar("N").Default(10);
      auto incdirs = o.Strs("incdir",   "include directory to search").Short('I');

      auto args = o.Parse(argc, argv);

      // print result of command linbe parsing
      if (*verbose) std::cout << "### verbose flag ON\n";
      if (*debug)   std::cout << "### debug flag ON\n";

      for (int i = 0; i < *repeat; i++) {
        std::cout << "### repeating " << i << "\n";
      }

      for (auto incdir : *incdirs) {
        std::cout << "### incdir: " << incdir << "\n";
      }
      return 0;
    }

This program can be built like this::

    $ clang++ demoprog.cpp -o demoprog
    $
    $ ./demoprog -h
    usage: demoprog [-h] [-v] [--debug] [--repeat N] [-I INCDIR]

    Demonstrate use of CxxArgparse

    optional arguments:
      -h, --help           show this help message and exit
      -v, --verbose        be more verbose
      --debug              enable debug output
      --repeat N           repeat things N times
      -I, --incdir INCDIR  include directory to search
    
From this example the following can be seen:

- the variable used to access the value of an option is a "pointer",
  and the value is retrieved by de-referencing the pointer.

- the value of options can be scalar values of type bool, int, or
  std::string. But arrays of ints och strings are also allowed.  The
  actual type of the ``--incdir`` option above is for example
  std::vector<std::string>.

- the output of the help message, is very similar to the output of
  Pythons ``argparse`` module.

