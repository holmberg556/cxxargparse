
#pragma once

#include <iostream>
#include <sstream>
#include <map>
#include <set>
#include <string>
#include <vector>
#include <memory>

namespace {

using string_arr_t = std::vector<std::string>;

std::string ToUpper(std::string s) {
  for (auto &c : s)
    c = toupper(c);
  return s;
}

//----------------------------------------------------------------------

string_arr_t ToVector(int argc, char *argv[]) {
  string_arr_t arr;
  for (int i = 0; i < argc; i++) {
    arr.push_back(argv[i]);
  }
  return arr;
}

//----------------------------------------------------------------------

struct Argv {
  string_arr_t args;

  Argv(int argc, char* argv[]) {
    args = ToVector(argc, argv);
  }
};

struct ArgvIterator {
  Argv & mArgv;
  int mIndex;

  ArgvIterator(Argv & xargs) : mArgv(xargs), mIndex(1) {}

  std::string & operator*() { return mArgv.args[mIndex]; }

  int nleft() { return mArgv.args.size() - mIndex; }

  ArgvIterator operator++(int) {
    ArgvIterator result(*this);
    mIndex++;
    return result;
  }
};

//----------------------------------------------------------------------

struct OptBase {
  std::string name;
  std::string desc;
  char Short;
  std::string group;
  std::string metavar;
  bool given = false;

  OptBase(std::string name, std::string desc) {
    this->name = name;
    this->desc = desc;
    this->Short = ' ';
  }

  virtual bool Parse(ArgvIterator &xargs, std::string &err) = 0;

  virtual std::string ShortFlagExample() = 0;
  virtual std::string LongFlagExample() = 0;

  std::string FlagExample() {
    return (ShortFlagExample() != "")  ? ShortFlagExample() : LongFlagExample();
  }

  std::string FlagExampleFull() {
    return (ShortFlagExample() != ""  ?
	    ShortFlagExample() + ", " + LongFlagExample() :
	    LongFlagExample());
  }

  std::string Description() {
    return desc;
  }

  std::string ShortFlag() {
    return Short == ' ' ? "" : "-" + std::string(1, Short);
  }

  std::string LongFlag() {
    return "--" + name;
  }

  std::string BothFlags() {
    return (ShortFlag() != "" ?
	    ShortFlag() + "/" + LongFlag() :
	    LongFlag());
  }

  bool FindArg(ArgvIterator &xargs, std::string &value, std::string & err) {
    std::string opt = "--" + this->name;
    if (*xargs == opt) {
      if (xargs.nleft() >= 2) {
	xargs++;
	value = *xargs++;
	return true;
      }
      else {
	err = "expected one argument";
	return true;
      }
    }
    else if ((*xargs).find(opt + "=") == 0) {
      value = (*xargs++).substr(opt.size() + 1);
      return true;
    }
    else {
      return false;
    }
  }
};

//----------------------------------------------------------------------

int StringToInt(std::string s, std::string &err)
{
  int res;
  try {
    res = stoi(s);
  }
  catch (std::invalid_argument & e) {
    err = "invalid int value: '" + s + "'";
    res = -1;
  }
  return res;
}

template <class T, class S> void AssignValue(T &t, S s, std::string &err);

template <> void AssignValue(int &t, std::string s, std::string &err) {
  t = StringToInt(s, err);
}

template <> void AssignValue(std::string &t, std::string s, std::string &err) { t = s; }

template <> void AssignValue(std::vector<std::string> &t, std::string s, std::string &err) {
  t.push_back(s);
}

template <> void AssignValue(std::vector<int> &t, std::string s, std::string &err) {
  t.push_back(StringToInt(s, err));
}

//----------------------------------------------------------------------

template <class T> struct Opt : public OptBase {
  T value;

  Opt(std::string name, std::string desc) : OptBase(name, desc) {}

  bool Parse(ArgvIterator &xargs, std::string &err) override {
    std::string svalue;
    if (FindArg(xargs, svalue, err)) {
      if (err != "") {
	err = "argument " + BothFlags() + ": " + err;
	return true;
      }
      else {
	AssignValue(this->value, svalue, err);
	if (err != "") {
	  err = "argument " + BothFlags() + ": " + err;
	}
	this->given = true;
	return true;
      }
    }
    else {
      return false;
    }
  }

  std::string ShortFlagExample() override {
    return (ShortFlag() != "") ? ShortFlag() + " " + Metavar() : "";
  }

  std::string LongFlagExample() override {
    return LongFlag() + " " + Metavar();
  }

  std::string Metavar() {
    if (this->metavar.empty()) {
      return ToUpper(this->name);
    }
    else {
      return this->metavar;
    }
  }

};

template <class T> struct OptPtr {
  Opt<T> *p;

  OptPtr(Opt<T> *p) { this->p = p; }

  operator bool() { return this->p->given; }

  T operator*() { return this->p->value; }

  OptPtr &Default(T arg) {
    this->p->value = arg;
    return *this;
  }

  OptPtr &Short(char c) {
    this->p->Short = c;
    return *this;
  }

  OptPtr &OneOf(std::string group) {
    this->p->group = group;
    return *this;
  }

  OptPtr &Metavar(std::string metavar) {
    this->p->metavar = metavar;
    return *this;
  }
};

//----------------------------------------------------------------------

template <> struct Opt<bool> : public OptBase {
  bool value;

  Opt(std::string name, std::string desc) : OptBase(name, desc) {
    this->value = false;
  }

  bool Parse(ArgvIterator &xargs, std::string &er) override {
    if (*xargs == "--" + this->name || (this->Short != ' ' && *xargs == std::string("-") + this->Short)) {
      this->value = true;
      this->given = true;
      xargs++;
      return true;
    }
    else {
      return false;
    }
  }

  std::string ShortFlagExample() override {
    return (ShortFlag() != "") ? ShortFlag() : "";
  }

  std::string LongFlagExample() override {
    return LongFlag();
  }

};

//----------------------------------------------------------------------

struct ArgBase {
  std::string name;
  std::string desc;
  char cardinality;
  bool given = false;

  virtual bool Parse(ArgvIterator &xargs, std::string &err) = 0;
};


template <class T>
struct Arg : public ArgBase {
  T value;

  Arg(std::string name, std::string desc) {
    this->name = name;
    this->desc = desc;
  }

  bool Parse(ArgvIterator &xargs, std::string &err) override {
    if (cardinality == '+') {
      if (xargs.nleft() == 0) {
	return false;
      }
      else {
	while (xargs.nleft() > 0) {
	  AssignValue(this->value, *xargs++, err);
	  this->given = true;
	}
      }
    }
    else if (cardinality == '*') {
      while (xargs.nleft() > 0) {
	AssignValue(this->value, *xargs++, err);
	this->given = true;
      }
    }
    else if (cardinality == '?') {
      if (xargs.nleft() > 0) {
	AssignValue(this->value, *xargs++, err);
	this->given = true;
      }
    }
    else {
      if (xargs.nleft() == 0) {
	return false;
      }
      else {
	AssignValue(this->value, *xargs++, err);
      }
    }
    return true;
  }

};

template <class T>
struct ArgPtr {
  Arg<T> *p;

  ArgPtr(Arg<T> *p) { this->p = p; }

  T operator*() { return this->p->value; }

  operator bool() { return this->p->given; }

  ArgPtr& Nargs(char cardinality) {
    this->p->cardinality = cardinality;
    return *this;
  }

};

//----------------------------------------------------------------------

struct ArgParser {
  std::string name;
  std::string desc;
  std::vector<OptBase *> opts;
  std::vector<ArgBase*> argObjs;
  std::vector<std::shared_ptr<ArgParser>> subcmds;
  std::shared_ptr<ArgParser> parent;

  ArgParser(std::string name, std::string desc) {
    this->name = name;
    this->desc = desc;
    this->parent = nullptr;
  }
};
  
struct ArgParserPtr {
  std::shared_ptr<ArgParser> p;
  
  ArgParserPtr(std::string name, std::string desc) {
    p = std::make_shared<ArgParser>(name, desc);
  }

  ArgParserPtr(std::shared_ptr<ArgParser> parser) {
    p = parser;
  }

  OptPtr<bool> Bool(std::string name, std::string desc = "") {
    auto arg = new Opt<bool>(name, desc);
    p->opts.push_back(arg);
    return OptPtr<bool>(arg);
  }

  OptPtr<int> Int(std::string name, std::string desc = "") {
    auto arg = new Opt<int>(name, desc);
    p->opts.push_back(arg);
    return OptPtr<int>(arg);
  }

  OptPtr<std::string> Str(std::string name, std::string desc = "") {
    auto arg = new Opt<std::string>(name, desc);
    p->opts.push_back(arg);
    return OptPtr<std::string>(arg);
  }

  OptPtr<std::vector<int>> Ints(std::string name, std::string desc = "") {
    auto arg = new Opt<std::vector<int>>(name, desc);
    p->opts.push_back(arg);
    return OptPtr<std::vector<int>>(arg);
  }

  OptPtr<std::vector<std::string>> Strs(std::string name,
                                        std::string desc = "") {
    auto arg = new Opt<std::vector<std::string>>(name, desc);
    p->opts.push_back(arg);
    return OptPtr<std::vector<std::string>>(arg);
  }

  ArgPtr<std::string> Arg(std::string name, std::string desc = "") {
    auto arg = new ::Arg<std::string>(name, desc);
    arg->cardinality = '1';
    p->argObjs.push_back(arg);
    return ArgPtr<std::string>(arg);
  }

  ArgPtr<std::vector<std::string>> Args(std::string name, std::string desc = "") {
    auto arg = new ::Arg<std::vector<std::string>>(name, desc);
    arg->cardinality = '+';
    p->argObjs.push_back(arg);
    return ArgPtr<std::vector<std::string>>(arg);
  }

  //--------------------------------------------------

  ArgParserPtr Subcmd(std::string name, std::string desc) {
    auto obj = std::make_shared<ArgParser>(name, desc);
    obj->parent = this->p;
    this->p->subcmds.push_back(obj);
    return ArgParserPtr(obj);
  }


  //--------------------------------------------------

  std::string FullName() {
    if (p->parent == nullptr) {
      return p->name;
    }
    else {
      return p->parent->name + " " + p->name;
    }
  }

  std::string TopName() {
    if (p->parent == nullptr) {
      return p->name;
    }
    else {
      return p->parent->name;
    }
  }


  //--------------------------------------------------

  bool ParseArg(ArgvIterator &xargs, std::string &err) {
    for (auto opt : p->opts) {
      if (opt->Parse(xargs, err)) {
        return true;
      }
    }
    return false;
  }

  // en try point
  string_arr_t Parse(int argc, char *argv[]) {
    auto xargs = Argv(argc, argv);
    ArgvIterator it(xargs);
    Parse(it);
    return xargs.args;
  }

  void Parse(ArgvIterator xargs) {
    if (false) {
      std::cout << "Parse() ..................\n";
      for (auto & x : p->opts) {
	std::cout << "Parse: " << p->name << ", OPT: " << x->name << "\n";
      }
      std::cout << "\n";
    }

    if (xargs.nleft() == 1 && *xargs == "-h") {
      PrintHelp(std::cout);
      exit(0);
    }

    string_arr_t unknown;

    while (xargs.nleft() > 0) {
      if (*xargs == "--") {
	xargs++;
        break;
      }
      if ((*xargs).substr(0, 1) != "-") {
        break;
      }

      std::string err;
      auto xxx = *xargs;
      bool found = ParseArg(xargs, err);
      //std::cout << "ParseArg: " << xxx << ", " << found << ", " << err << "\n";
      if (found && err != "") {
        PrintUsage(std::cerr);
        std::cerr << FullName() << ": error: " << err << "\n";
        exit(2);
      }
      if (found) continue;

      unknown.push_back(*xargs++);
    }

    std::map<std::string,std::set<std::string>> oneOf;
    for (auto & opt : p->opts) {
      if (opt->given && opt->group != "") {
        oneOf[opt->group].insert("--" + opt->name);
      }
    }
    for (auto & kv : oneOf) {
      if (kv.second.size() > 1) {
        std::cout << "ERROR: more than one of: ";
        for (auto & x : kv.second) {
          std::cout << " " << x;
        }
        std::cout << "\n";
        exit(1);
      }
    }


    string_arr_t missing;

    int i;
    bool ok = true;
    for (i = 0; i < p->argObjs.size(); i++) {
      std::string err;
      ok = ok && p->argObjs[i]->Parse(xargs, err);
      if (! ok) {
        missing.push_back(p->argObjs[i]->name);
      }
    }

    if (missing.size() > 0) {
      PrintUsage(std::cerr);
      std::cerr << p->name << ": error: the following arguments are required: ";
      for (int i=0; i<missing.size(); i++) {
        if (i > 0) std::cerr << ", ";
        std::cerr << missing[i];
      }
      std::cerr << "\n";
      exit(2);
    }

    if (p->subcmds.size() > 0) {
      //std::cerr << "...3\n";
      if (xargs.nleft() > 0) {
	std::string subcmd_name = *xargs++;
	std::shared_ptr<ArgParser> subcmd = nullptr;
	for (auto & x : p->subcmds) {
	  if (x->name == subcmd_name) {
	    subcmd = x;
	    break;
	  }
	}
	//std::cerr << ".... " << subcmd_name << "\n";
	if (subcmd != nullptr) {
	  if (unknown.size() == 0) {
	    //std::cerr << "**** " << subcmd_name << "***\n";
	    ArgParserPtr(subcmd).Parse(xargs);
	    return;
	  }
	  else {
	    // pass through
	  }
	}
	else {
	  PrintUsage(std::cerr);
	  std::cerr << p->name << ": error: invalid choice: '" << subcmd_name << "' (choose from ";
	  bool first = true;
	  for (auto & x : p->subcmds) {
	    if (!first) {
	      std::cerr << ", ";
	    }
	    first = false;
	    std::cerr << "'" << x->name << "'";
	  }
	  std::cerr << ")\n";
	  exit(2);
	}
      }
    }

    while(xargs.nleft() > 0) {
      unknown.push_back(*xargs++);
    }

    if (unknown.size() > 0) {
      PrintUsage(std::cerr);
      std::cerr << TopName() << ": error: unrecognized arguments: ";
      for (int i=0; i<unknown.size(); i++) {
        if (i > 0) std::cerr << " ";
        std::cerr << unknown[i];
      }
      std::cerr << "\n";
      exit(2);
    }
  }

  void PrintUsage(std::ostream & std_cxxx) {
    std::ostringstream out;
    out << "usage: ";
    if (p->parent != nullptr) {
      out << p->parent->name << " ";
    }
    out << p->name;
    auto indent = out.str().size();
    out << " [-h]";

    auto append = [&](std::string s) {
      if (out.str().size() + 1 + s.size() > 78) {
        std_cxxx << out.str() << "\n";
        out.str("");
      }
      if (out.str().size() == 0) {
        out << std::string(indent, ' ');
      }
      out << " " << s;
    };

    for (auto &opt : p->opts) {
      auto s = "[" + opt->FlagExample() + "]";
      append(s);
    }

    if (p->argObjs.size() > 0) {
      std::string argsStr;
      for (auto &arg : p->argObjs) {
        if (argsStr != "") argsStr += " ";
	if (arg->cardinality == '+') {
	  argsStr += arg->name + " [" + arg->name + " ...]";
	}
	else if (arg->cardinality == '*') {
	  argsStr += "[" + arg->name + " [" + arg->name + " ...]]";
	}
	else if (arg->cardinality == '?') {
	  argsStr += "[" + arg->name + "]";
	}
	else {
	  argsStr += arg->name;
	}
      }
      append(argsStr);
    }

    if (p->subcmds.size() > 0) {
      out << " {";
      int i = 0;
      for (auto subcmd : p->subcmds) {
	if (i++ != 0) {
	  out << ",";
	}
	out << subcmd->name;
      }
      out << "} ...";
    }

    std_cxxx << out.str() << "\n";
  }

  void PrintHelp(std::ostream & std_cxxx) {
    PrintUsage(std_cxxx);
    std_cxxx << "\n";

    if (p->parent == nullptr) {
      std_cxxx << p->desc << "\n";
      std_cxxx << "\n";
    }

    if (p->argObjs.size() > 0 || p->subcmds.size() > 0) {
      std_cxxx << "positional arguments:\n";
      for (auto &arg : p->argObjs) {
        std_cxxx << "  " << arg->name
                  << std::string(15 - arg->name.size(), ' ') << arg->desc
                  << "\n";
      }

      if (p->subcmds.size() > 0) {

	std_cxxx << " {";
	int i = 0;
	for (auto subcmd : p->subcmds) {
	  if (i++ != 0) {
	    std_cxxx << ",";
	  }
	  std_cxxx << subcmd->name;
	}
	std_cxxx << "}\n";

	for (auto & subcmd : p->subcmds) {
	  std_cxxx << "    " << subcmd->name
		   << std::string(13 - subcmd->name.size(), ' ') << subcmd->desc
		   << "\n";
	}
      }

      std_cxxx << "\n";
    }

    std_cxxx << "optional arguments:\n";
    string_arr_t flags;
    string_arr_t descs;

    flags.push_back("-h, --help");
    descs.push_back("show this help message and exit");

    for (auto opt : p->opts) {
      flags.push_back(opt->FlagExampleFull());
      descs.push_back(opt->Description());
    }

    int maxFlagLen = 0;
    for (auto & s : flags) {
      maxFlagLen = (s.size() > maxFlagLen) ? s.size() : maxFlagLen;
    }

    const int MAXINDENT = 22;
    const int NSPACES = 2;
    int pos = (maxFlagLen >= MAXINDENT - NSPACES) ? MAXINDENT : maxFlagLen + NSPACES;
    for (int i=0; i<flags.size(); i++) {
      std::string line = "  " + flags[i];
      std::string word;
      std::istringstream words(descs[i]);
      if (line.size() > pos) {
        std_cxxx << line << "\n";
        line = "";
      }
      while(words >> word) {
        if (line.size() <= pos) {
          line += std::string(pos - line.size() + NSPACES, ' ') + word;
        }
        else if (line.size() + 1 + word.size() <= 78) {
          line += " " + word;
        }
        else {
          std_cxxx << line << "\n";
          line = std::string(pos + NSPACES, ' ') + word;
        }
      }
      if (line.size() > 0) {
        std_cxxx << line << "\n";
      }
    }

  }
};

using CxxArgparse = ArgParserPtr;

} // namespace
